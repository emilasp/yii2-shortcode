Модуль Short Code для Yii2
=============================

Данное расширение добавляет возможность подключения, добавления и парсинга shortcode.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add to composer.json:
```json
"emilasp/yii2-shortcode": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-shortcode.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем в конфиг приложения модуль:

```php
'modules' => [
        'shortcode' => [
                    'class' => 'emilasp\shortcode\Module',
                ],
        ...
    ],
```


2) Добавляем виджет в форму:
```php
echo emilasp\shortcode\shortcodes\ImageInsert\backend\ImageInsertWidget::widget();
```
3) Добавляем поведение в модель:
```php
public function behaviors()
    {
        return [
            [
                'class' => \emilasp\category\behaviors\CategoryBehavior::className(),
                'type'=>OptionsData::CATEGORY_TYPE_POST
            ],
            [
                'class' => \emilasp\shortcode\behaviors\ShortCodeBehavior::className(),
                'shorts' => [
                    'emilasp\shortcode\shortcodes\ImageInsert\Codes\ImageInsert'=>['attributes'=>['description2','description']],
                ],
                'param'=>[ ],
            ],
        ];
    }
```

4)

