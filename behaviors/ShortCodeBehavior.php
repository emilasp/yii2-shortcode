<?php

namespace emilasp\shortcode\behaviors;

use yii\base\Behavior;
use yii\helpers\ArrayHelper;


/**
 * Shortcode ActiveRecord Behavior
 *
 *
 * 1. Add to model
 * public function behaviors()
 * {
 *     return ArrayHelper::merge([
 *         [
 *             'class'      => ShortCodeBehavior::class,
 *             'attributes' => ['content'],
 *             'shortCodes' => [
 *                 'document'    => [
 *                     'class'   => ContentDocumentWidget::class,
 *                 ],
 *             ]
 *         ],
 *     ], parent::behaviors());
 * }
 *
 * 2. After find
 * $model->applyShortCodes();
 *
 * 3. In content
 * <document id="1" /> | <document id="1"></document>
 *
 *
 * Class ShortCodeBehavior
 * @package emilasp\shortcode\behaviors
 */
class ShortCodeBehavior extends Behavior
{
    public const TYPE_WIDGET = 1;

    public $attributes = [];
    public $shortCodes = [];

    /**
     * Применяем шорткоды
     */
    public function applyShortCodes(): void
    {
        foreach ($this->attributes as $attribute) {
            $shortCodes = $this->parseShortCodeTags($attribute);

            $this->setShortCodes($attribute, $shortCodes);
        }
    }

    /**
     * Применяем шорткод
     *
     * @param string $attribute
     * @param array  $shortCodes
     * @return mixed
     */
    private function setShortCodes($attribute, $shortCodes): void
    {
        $content   = $this->owner->{$attribute};

        foreach ($shortCodes as $shortCode) {
            $params = $this->shortCodes[$shortCode['shortcode']];

            $type      = $params['type'] ?? self::TYPE_WIDGET;
            $className = $params['class'];
            $options   = $params['options'] ?? [];

            if ($className && class_exists($className)) {
                switch ($type) {
                    case self::TYPE_WIDGET:
                        $codeContent = $className::widget(ArrayHelper::merge($options, $shortCode['options']));
                        break;
                }

                $content = str_replace($shortCode['html'], $codeContent, $content);
            }
        }

        $this->owner->{$attribute} = $content;
    }

    /**
     * Парсим шорткод
     *
     * @param string $attribute
     * @return array
     */
    private function parseShortCodeTags(string $attribute): array
    {
        $shortCodes = [];

        $content    = $this->owner->{$attribute};
        $tagsString = implode('|', array_keys($this->shortCodes));

        preg_match_all("/<({$tagsString})(.*?)[^>]\/>|<({$tagsString})(.*?)><\/{$tagsString}>/", $content, $out);

        foreach ($out[0] as $index => $html) {
            $shortCodes[] = [
                'shortcode' => $out[1][$index] ?: $out[3][$index],
                'html'      => $html,
                'options'   => $this->getTagAttributes($html)
            ];
        }

        return $shortCodes;
    }

    /**
     * Получаем атрибуты для тега
     *
     * @param string $htmlTag
     * @return array
     */
    private function getTagAttributes(string $htmlTag): array
    {
        $xml = simplexml_load_string("{$htmlTag}");
        return ((array)$xml->attributes())["@attributes"] ?? [];
    }
}
